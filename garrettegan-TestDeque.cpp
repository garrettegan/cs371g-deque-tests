// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;};

using
    deque_types =
    Types<
           deque<int>,
        my_deque<int>,
           deque<int, allocator<int>>,
        my_deque<int, allocator<int>>>;

#ifdef __APPLE__
    TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
    TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----


// --------
// Iterator
// --------

#define TEST_CHUNK_SIZE 250

TYPED_TEST(DequeFixture, iteratorEqualsEmpty) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b, e);}

TYPED_TEST(DequeFixture, iteratorNotEqualsNonEmpty) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(1);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_NE(b, e);}

TYPED_TEST(DequeFixture, iteratorPreincriment) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(1);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(++b, e);}

TYPED_TEST(DequeFixture, iteratorPostincriment) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(1);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_NE(b++, e);
    ASSERT_EQ(b, e);}

TYPED_TEST(DequeFixture, iteratorPredecriment) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(1);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b, --e);}

TYPED_TEST(DequeFixture, iteratorPostdecriment) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(1);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_NE(b, e--);
    ASSERT_EQ(b, e);}

TYPED_TEST(DequeFixture, iteratorPlus) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(2);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b + 2, e);}

TYPED_TEST(DequeFixture, iteratorPlusNegative) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(2);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b, e - 2);}

TYPED_TEST(DequeFixture, iteratorPlusChunkSize) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(TEST_CHUNK_SIZE);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b + TEST_CHUNK_SIZE, e);}

TYPED_TEST(DequeFixture, iteratorPlusChunkSizeNegative) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(TEST_CHUNK_SIZE);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b, e - TEST_CHUNK_SIZE);}

TYPED_TEST(DequeFixture, iteratorMinus) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(2);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b, e - 2);}

TYPED_TEST(DequeFixture, iteratorMinusNegative) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(2);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b - -2, e);}
    
TYPED_TEST(DequeFixture, iteratorMinusChunkSize) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(TEST_CHUNK_SIZE);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b, e - TEST_CHUNK_SIZE);}

TYPED_TEST(DequeFixture, iteratorMinusChunkSizeNegative) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(TEST_CHUNK_SIZE);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b - -TEST_CHUNK_SIZE, e);}

TYPED_TEST(DequeFixture, iteratorDeref) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(1,5);
    iterator b = begin(x);
    ASSERT_EQ(*b, 5);}

// --------------
// Const Iterator
// --------------

TYPED_TEST(DequeFixture, constIteratorEqualsEmpty) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(b, e);}

TYPED_TEST(DequeFixture, constIteratorNotEqualsNonEmpty) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(1);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_NE(b, e);}

TYPED_TEST(DequeFixture, constIteratorPreincriment) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(1);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(++b, e);}

TYPED_TEST(DequeFixture, constIteratorPostincriment) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(1);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_NE(b++, e);
    ASSERT_EQ(b, e);}

TYPED_TEST(DequeFixture, constIteratorPredecriment) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(1);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(b, --e);}

TYPED_TEST(DequeFixture, constIteratorPostdecriment) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(1);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_NE(b, e--);
    ASSERT_EQ(b, e);}

TYPED_TEST(DequeFixture, constIteratorPlus) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(2);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(b + 2, e);}

TYPED_TEST(DequeFixture, constIteratorPlusNegative) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(2);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(b, e - 2);}

TYPED_TEST(DequeFixture, constIteratorPlusChunkSize) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(TEST_CHUNK_SIZE);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(b + TEST_CHUNK_SIZE, e);}

TYPED_TEST(DequeFixture, constIteratorPlusChunkSizeNegative) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(TEST_CHUNK_SIZE);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(b, e - TEST_CHUNK_SIZE);}

TYPED_TEST(DequeFixture, constIteratorMinus) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(2);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(b, e - 2);}

TYPED_TEST(DequeFixture, constIteratorMinusNegative) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(2);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(b - -2, e);}
    
TYPED_TEST(DequeFixture, constIteratorMinusChunkSize) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(TEST_CHUNK_SIZE);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(b, e - TEST_CHUNK_SIZE);}

TYPED_TEST(DequeFixture, constIteratorMinusChunkSizeNegative) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(TEST_CHUNK_SIZE);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(b - -TEST_CHUNK_SIZE, e);}

TYPED_TEST(DequeFixture, constIteratorDeref) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(1,5);
    const_iterator b = begin(x);
    ASSERT_EQ(*b, 5);}

// -----
// Deque
// -----


TYPED_TEST(DequeFixture, equalityTrue) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(2, 4);
    deque_type y(2, 4);
    ASSERT_EQ(x, y);}

TYPED_TEST(DequeFixture, equalityFalseSize) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(2, 4);
    deque_type y(3, 4);
    ASSERT_NE(x, y);}

TYPED_TEST(DequeFixture, equalityFalseValues) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(2, 4);
    deque_type y(2, 3);
    ASSERT_NE(x, y);}

TYPED_TEST(DequeFixture, lessLength) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(2, 3);
    deque_type y(3, 3);
    ASSERT_LT(x, y);}

TYPED_TEST(DequeFixture, lessLexagraphical) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(3, 1);
    deque_type y(2, 3);
    ASSERT_LT(x, y);}

TYPED_TEST(DequeFixture, assignmentSame) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(2, 4);
    ASSERT_EQ(&x, &(x = x));}

TYPED_TEST(DequeFixture, assignmentSameSize) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(2, 4);
    deque_type y(2, 5);
    x = y;
    auto it = x.begin();
    ASSERT_EQ(*it++, 5);
    ASSERT_EQ(*it, 5);}

TYPED_TEST(DequeFixture, assignmentSmaller) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(27, 4);
    deque_type y(2, 5);
    x = y;
    auto it = x.begin();
    ASSERT_EQ(*it++, 5);
    ASSERT_EQ(*it, 5);
    ASSERT_EQ(x.size(), y.size());}

TYPED_TEST(DequeFixture, assignmentInCapacity) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(1, 4);
    deque_type y(2, 5);
    x = y;
    auto it = x.begin();
    ASSERT_EQ(*it++, 5);
    ASSERT_EQ(*it, 5);
    ASSERT_EQ(x.size(), y.size());}

TYPED_TEST(DequeFixture, assignmentGrowth) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(1, 4);
    deque_type y(2 * TEST_CHUNK_SIZE, 5);
    x = y;
    ASSERT_EQ(*x.begin(), 5);
    ASSERT_EQ(*(x.begin() + (y.size() - 1)), 5);
    ASSERT_EQ(x.size(), y.size());}

TYPED_TEST(DequeFixture, index) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x[1], 2);
    ASSERT_EQ(x[2], 3);
    ASSERT_EQ(x[3], 4);}

TYPED_TEST(DequeFixture, at) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    ASSERT_EQ(x.at(0), 1);
    ASSERT_EQ(x.at(1), 2);
    ASSERT_EQ(x.at(2), 3);
    ASSERT_EQ(x.at(3), 4);}

TYPED_TEST(DequeFixture, atOutOfBoundsNeg) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    ASSERT_THROW(x.at(-1), std::out_of_range);}

TYPED_TEST(DequeFixture, atOutOfBoundsSize) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    ASSERT_THROW(x.at(x.size()), std::out_of_range);}

TYPED_TEST(DequeFixture, back) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    ASSERT_EQ(x.back(), 4);}

TYPED_TEST(DequeFixture, begin) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    ASSERT_EQ(*x.begin(), 1);}

TYPED_TEST(DequeFixture, clear) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    x.clear();
    ASSERT_EQ(x.size(), 0);}

TYPED_TEST(DequeFixture, emptyTrue) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());}

TYPED_TEST(DequeFixture, emptyFalse) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(2);
    ASSERT_FALSE(x.empty());}

TYPED_TEST(DequeFixture, end) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    ASSERT_EQ(*(x.end() - 1), 4);}

TYPED_TEST(DequeFixture, eraseEnd) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    auto end = x.end();
    ASSERT_EQ(x.erase(end), end);}

TYPED_TEST(DequeFixture, eraseLast) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    auto last = x.end() - 1;
    ASSERT_EQ(x.erase(last), x.end() - 1);}

TYPED_TEST(DequeFixture, eraseFirst) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    auto first = x.begin();
    ASSERT_EQ(x.erase(first), x.begin() + 1);}

TYPED_TEST(DequeFixture, eraseMiddle) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    auto element = x.begin() + 2;
    ASSERT_EQ(x.erase(element), x.begin() + 2);
    ASSERT_EQ(x.at(0), 1);
    ASSERT_EQ(x.at(1), 2);
    ASSERT_EQ(x.at(2), 4);
    ASSERT_EQ(x.size(), 3);}

TYPED_TEST(DequeFixture, front) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    ASSERT_EQ(x.front(), 1);}

TYPED_TEST(DequeFixture, insertBack) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    x.insert(x.end(), 12);
    ASSERT_EQ(x.back(), 12);
    ASSERT_EQ(x.size(), 5);}

TYPED_TEST(DequeFixture, insertFront) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    x.insert(x.begin(), 12);
    ASSERT_EQ(x.front(), 12);
    ASSERT_EQ(x.size(), 5);}

TYPED_TEST(DequeFixture, insertMiddle) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    x.insert(x.begin() + 2, 12);
    ASSERT_EQ(x.at(0), 1);
    ASSERT_EQ(x.at(1), 2);
    ASSERT_EQ(x.at(2), 12);
    ASSERT_EQ(x.at(3), 3);
    ASSERT_EQ(x.at(4), 4);
    ASSERT_EQ(x.size(), 5);}

TYPED_TEST(DequeFixture, popBack) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    x.pop_back();
    ASSERT_EQ(x.back(), 3);
    ASSERT_EQ(x.size(), 3);}

TYPED_TEST(DequeFixture, popFront) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    x.pop_front();
    ASSERT_EQ(x.front(), 2);
    ASSERT_EQ(x.size(), 3);}

TYPED_TEST(DequeFixture, pushBack) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    x.push_back(5);
    ASSERT_EQ(x.back(), 5);
    ASSERT_EQ(x.size(), 5);}

TYPED_TEST(DequeFixture, pushFront) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    x.push_front(5);
    ASSERT_EQ(x.front(), 5);
    ASSERT_EQ(x.size(), 5);}

TYPED_TEST(DequeFixture, resizeSame) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    x.resize(4);
    ASSERT_EQ(x.at(0), 1);
    ASSERT_EQ(x.at(1), 2);
    ASSERT_EQ(x.at(2), 3);
    ASSERT_EQ(x.at(3), 4);
    ASSERT_EQ(x.size(), 4);}

TYPED_TEST(DequeFixture, resizeSmaller) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    x.resize(2);
    ASSERT_EQ(x.at(0), 1);
    ASSERT_EQ(x.at(1), 2);
    ASSERT_EQ(x.size(), 2);}

TYPED_TEST(DequeFixture, resizeInCapacity) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    x.resize(5, 11);
    ASSERT_EQ(x.at(0), 1);
    ASSERT_EQ(x.at(1), 2);
    ASSERT_EQ(x.at(2), 3);
    ASSERT_EQ(x.at(3), 4);
    ASSERT_EQ(x.at(4), 11);
    ASSERT_EQ(x.size(), 5);}

TYPED_TEST(DequeFixture, resizeLarger) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4});
    x.resize(2 * TEST_CHUNK_SIZE, 9);
    ASSERT_EQ(x.at(0), 1);
    ASSERT_EQ(x.at(1), 2);
    ASSERT_EQ(x.at(2), 3);
    ASSERT_EQ(x.at(3), 4);
    ASSERT_EQ(x.at(4), 9);
    ASSERT_EQ(x.at(2 * TEST_CHUNK_SIZE - 1), 9);
    ASSERT_EQ(x.size(), 2 * TEST_CHUNK_SIZE);}

TYPED_TEST(DequeFixture, sizeZero) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_EQ(x.size(), 0u);}

TYPED_TEST(DequeFixture, sizeNonZero) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(3);
    ASSERT_EQ(x.size(), 1u);}

TYPED_TEST(DequeFixture, swapSame) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({5, 4, 3});
    deque_type& y = x;
    size_t preSwapSize = x.size();
    x.swap(y);
    ASSERT_EQ(x.size(), preSwapSize);}

TYPED_TEST(DequeFixture, swapDifferent) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({5, 4, 3});
    deque_type y(TEST_CHUNK_SIZE * 4, 8);
    size_t xPreSwapSize = x.size();
    size_t yPreSwapSize = y.size();
    x.swap(y);
    ASSERT_EQ(x.front(), 8);
    ASSERT_EQ(x.back(), 8);
    ASSERT_EQ(x.size(), yPreSwapSize);
    ASSERT_EQ(y.front(), 5);
    ASSERT_EQ(y.back(), 3);
    ASSERT_EQ(y.size(), xPreSwapSize);}
